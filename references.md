Brentari, Mirko, Paolo Bosetti, Isabelle Queinnec, a Luca Zaccarian. „Benchmark model of Quanser’s 3 DOF Helicopter". LAAS CNRS, February 2018. https://hal.laas.fr/hal-01711135.

Obrusník, Vít. Quanser 3 dof helicopter laboratory model. Czech Technical University in Prague, 2018. Source code repository https://github.com/obrusvit/quanser-3dof-heli.