# Software

- `quanser_original` is the code that can be obtained on the [dedicated page on Quanser website](https://www.quanser.com/products/3-dof-helicopter/). Just for archiving purposes here. Do not use!
- `quanser_fixed` is the Quanser code after some modifications and upgrades useful or even necessary for our setup. In particular, the data acquisition board was set correctly to our `Q8 USB` DAQ, the configuration file `quanser_win64.tlc` for 64-bit Windows was correctly set, the Simulink files were saved in the new SLX format (corresponding to R2021a), and some irrelevant files were removed for better clarity. 
