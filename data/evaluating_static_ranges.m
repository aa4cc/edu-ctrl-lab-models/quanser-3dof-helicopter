%% Pitch ranges

pitch_min = min(pitch_data); pitch_min = pitch_min(2)
pitch_max = max(pitch_data); pitch_max = pitch_max(2)
pitch_range = pitch_max-pitch_min

%% Elevation ranges

elevation_min = min(elevation_data); elevation_min = elevation_min(2)
elevation_max = max(elevation_data); elevation_max = elevation_max(2)
elevation_range = elevation_max-elevation_min

elevation_level = 15.8 % also elev_0, determined manually using spirit level
elevation_low = -19.4
elevation_high = 49.7
