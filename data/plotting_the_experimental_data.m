figure
subplot(4,1,1)
plot(travel_data(:,1),travel_data(:,2),LineWidth=2)
xlabel("Time [s]")
ylabel("Travel [deg]")
grid on

subplot(4,1,2)
plot(elevation_data(:,1),elevation_data(:,2),LineWidth=2)
xlabel("Time [s]")
ylabel("Elevation [deg]")
grid on

subplot(4,1,3)
plot(pitch_data(:,1),pitch_data(:,2),LineWidth=2)
xlabel("Time [s]")
ylabel("Pitch [deg]")
grid on

subplot(4,1,4)
plot(voltage_data(:,1),voltage_data(:,2:3),LineWidth=2)
xlabel("Time [s]")
ylabel("Voltage [V]")
grid on