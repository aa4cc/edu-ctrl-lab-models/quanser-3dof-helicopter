# Parameters

Just the parameteres relevant for control design.

## Components

- Brush-type DC motors Pitman ([reportedly](https://www.lehigh.edu/~inconsy/lab/frames/experiments/QUANSER-3DOFHelicopter_Reference_Manual.pdf) 9234, do not seem to be available online with their data sheets)
- Incremental encoders
- Propellers from [Graupner](https://www.graupner.com/PROPELLERS_c_177.html) 

## Parameters from data sheet(s)

| Name of the parameter  	                                 |   Value     | Unit   |
|:---------------------------------------------------------|:------------|:-------|
| Pitch and elevation encoders resolution (in quadrature)  |  4096 		   | counts/rev
| Yaw/travel encoder resolution (in quadrature) 				   |  8192 		   | counts/rev
| Pitch angle range 											                 |  64 ± 32.0	 | deg
| Elevation angle range 										               |  63.5 		   | deg
| Travel angle range 										                   |  360 		   | deg
| Pitch force thrust constant 								             |  0.22   	   | N/V
| Propeller diameter 										                   |  20.3 		   | cm
| Propeller pitch 											                   |  15.2 		   | cm
| Pitch/front motor resistance 								             |  0.83 		   | Ω
| Pitch/front motor – current-torque constant 				     |  0.0182 	   | Nm/A
| Yaw/back motor resistance 									             |  0.83 		   | Ω
| Yaw/back motor – current-torque constant 					       |  0.0182 	   | Nm/A


## Parameters measured without running the experiment

| Name of the parameter  	                                 |   Value     | Unit   |
|:---------------------------------------------------------|:------------|:-------|
| Mass of the counterweight  |  ? 		   | kg


## Parameters set in software

| Name of the parameter  	                                 |   Value     | Unit   |
|:---------------------------------------------------------|:------------|:-------|
| Sampling period  |  0.002 		   | s