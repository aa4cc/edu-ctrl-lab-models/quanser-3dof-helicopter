# Description of the measured data sets

Several simple open-loop experiments were conducted. Either completely without commanding the motors or with just some voltage steps, stairs or PRBS sequences. For all the experiments, their corresponding mat-files contain four arrays (matrices)

- `travel_data`
- `elevation_data`
- `pitch_data`
- `voltage_data` 

and each array contains as the first column the time vector, while the second column (and the third for `voltage_data`) are the corresponding variables (angles and voltages). Load the data to the Matlab workspace using `load` command. For example

```matlab
load('elevation_stair_response_2')
```
creates the following 

```matlab
>> whos
  Name                     Size              Bytes  Class     Attributes

  elevation_data      240705x2             3851280  double              
  pitch_data          240705x2             3851280  double              
  travel_data         240705x2             3851280  double              
  voltage_data        240705x3             5776920  double
```

The data can be quickly visualized using the following code:

```matlab
figure
subplot(4,1,1)
plot(travel_data(:,1),travel_data(:,2))
xlabel("Time [s]")
ylabel("Travel [deg]")
grid on

subplot(4,1,2)
plot(elevation_data(:,1),elevation_data(:,2))
xlabel("Time [s]")
ylabel("Elevation [deg]")
grid on

subplot(4,1,3)
plot(pitch_data(:,1),pitch_data(:,2))
xlabel("Time [s]")
ylabel("Pitch [deg]")
grid on

subplot(4,1,4)
plot(voltage_data(:,1),voltage_data(:,2:3))
xlabel("Time [s]")
ylabel("Voltage [V]")
grid on
```
which for this particular data set yields

![Elevation stair response](../figures/elevation_stair_response_2.png)

Sampling period common for all experiments and all measurements was 

```matlab
>> Ts = elevation_data(2,1)-elevation_data(1,1)

Ts =

    0.0020
```

The experiments were:

- `measuring_static_ranges.mat` – voltages off, helicopter still at the beginning, elevation manually moved to the lowest value, then moved to the highest value, then moved to the level orientation of the arm (measured using a [spirit level](https://en.wikipedia.org/wiki/Spirit_level)), then the arm moved clockwise, wait, move back counterclockwise, finally the front motor initially oriented down is moved the uppermost orientation, wait and back to the nose down orientation. The accompanying simple script `analyzing_static_ranges.m` computes the ranges for the elevation an pitch angles. 
- `elevation_initial_response_1.mat` and `elevation_initial_response_2.mat` – recorded responses to the nonzero initial conditions for the elevation angle, namely the arm push to the lowest elevation and then released. Two recordings provided. The pitch was fixed using a tape to the level orientation.
- `elevation_step_response_3V_1.mat`, `elevation_step_response_3V_2.mat`, `elevation_step_response_5V_1.mat`, `elevation_step_response_5V_2.mat`, and `elevation_step_response_5V_3.mat` – recorded responses to step changes in both voltages (two runs for 3V steps, three runs for 5V steps). The pitch was fixed using a tape to the level orientation.
- `elevation_stair_response_1.mat` through `elevation_stair_response_5.mat` – recorded responses to stair (staircase-like) changes in both voltages (several runs). The pitch was fixed using a tape to the level orientation, still for higher voltage the small deviation of the pitch angle from the level orientation caused a slow travel. For some later experiments the vertical axis was held fixed manually to prevent the helicopter from traveling due to not perfectly levelled pitch.
- `elevation_negative_stair_response_1.mat` through `elevation_negative_stair_response_3.mat` – the same as above but for negative staircase-like voltages.
- `elevation_PRBS_U4_B4_1.mat` through `elevation_PRBS_U8_B4_3.mat` – recorded responses to PRBS voltages (identical on both motors). The `U4` through `U8` identifier tells the base voltage (4V through 8V) onto which a PRBS voltage is superposed (the `Range` was fixed to ±3V). The identifier `B4` encodes the inverse of the `B` parameter in the vector parameter `Band = [0, B]` in the [idinput](https://www.mathworks.com/help/ident/ref/idinput.html) function, that is, `B = 1/4`. In turn, this means that the clock period of the PRBS signal – the minimum number of sampling intervals for which the value of the signal does not change – is 4. It is known that for this choice the frequency band(width) covered by the PRBS signal is some 1/5 of the Nyquist frequency, that is some 1/5 × 1/2 × 500 Hz = 50  Hz in this case. The final number `_1` through `_5` (or so) in the names only indicates a run. Note that in principle the runs should be identical since the PRBS input is (it is not random), but in reality the runs were not iniated in perfectly identical initial elevation (just manually supported). Uploaded is also a script `sys_id_experiments.m` for generating the inputs and plotting the stored responses. 
- `pitch_step_response_front_motor_5V.mat`, and `pitch_step_response_front_motor_6V` – recorded responses to steps (5 an 6V) on the front motor while the arm was manually kept at level orientation.
- `pitch_negative_step_response_front_motor_10V` – the same as above but for negative voltage on the fron motor.
- `travel_initial_response_1.mat` through `travel_initial_response_5.mat` – recorded response of the travel angle to some nonzero initial velocity. The arm was set to motion just manually, trying not to induce much elevation oscillation (but not succeeding perfectly). For the runs 3 through 5 the arm was set to motion by pushing the central joint, hence the elevation oscilations are truly induced just by the dynamic coupling between the axes. The pitch was fixed using a tape to the level orientation. 

