# Quanser 3DOF Helicopter

This is a repository with supporting material (texts, data, codes, ...) for [Quanser 3DOF Helicopter](https://www.quanser.com/products/3-dof-helicopter/).

![Quanser 3DOF Helicopter](figures/3-DOF-Helicopter_with_descriptions.png)

One way to interpret this laboratory/toy gadget is this that it represents an anchored (and simplified) two-rotor helicopter such as the famous [CH-47 Chinook](https://en.wikipedia.org/wiki/Boeing_CH-47_Chinook) 
![Quanser 3DOF Helicopter](figures/CH-47_Chinook_helicopter_flyby.jpg)

Source: [https://en.wikipedia.org/wiki/Boeing_CH-47_Chinook#/media/File:CH-47_Chinook_helicopter_flyby.jpg](https://en.wikipedia.org/wiki/Boeing_CH-47_Chinook#/media/File:CH-47_Chinook_helicopter_flyby.jpg)

## Control systems challenge

From a control systems perspective, this sort-of-kind-of Chinook helicopter has two independent actuators – propellers driven by DC motors. Thrust induced by the two propellers together with the [**pitch**](https://en.wikipedia.org/wiki/Aircraft_principal_axes#Transverse_axis_(pitch)) angle (the angle of the Chinook around the transverse axis), determine both the vertical and the forward motion. Since the helicopter is suspended by a long arm anchored at a 2-DOF joint, the former motion exhibits itself as an **elevation angle** of the arm, while the latter exhibits itself as a **travel angle** (angle around a vertical axis) of the same arm. Designing a **controller that tracks reference values of both the elevation and the travel angles by setting the voltages applied to the two DC motors while measuring the three angles (pitch, elevation, travel) using incremental encoders** constitutes the control challenge with this laboratory gadget.

![Input-output block diagram](figures/input_output_block_diagram.png)

## Instructions for running the experiment

1. Double-check that the room around the experiment is free of any obstacles.

2. The only possibility to change the configuration of the experiment is to change the location of the counterweight block (the brass-ish cube). You are certainly not expected to experiment with this setting, but just check that the fastening screw is in the fifth hole (on the top side this is labelled as corresponding to 71 g of equivalent mass on the helicopter side). 

3. Switch on the [VoltPAQ-X2 Amplifier](https://www.quanser.com/products/voltpaq-x2-amplifier/) – the switch is on the back side just above the power cable. Check that the gains are correctly switched to `3x` on the front panel (just in case somebody changed this setting before you).

4. Connect the [Q8-USB Data Acquisition Device](https://www.quanser.com/products/q8-usb-data-acquisition-device/) to the power (it has no switch, just plug in the connector on the back side).

5. If you have not done so, download the code from our repository here, namely from [/sotware/quanser-fixed2021](https://gitlab.fel.cvut.cz/aa4cc/edu-ctrl-lab-models/quanser-3dof-helicopter/-/tree/master/software/quanser-fixed2021) subdirectory. Note that you should certainly pick this directory and not the directory with the original code (that you can also download from [the product webpage](https://www.quanser.com/products/3-dof-helicopter/) as the Simulink Courseware) because the latter contains a few problems that have already been fixed in our version.

6. Launch Matlab R2021a.  

7. In Matlab switch to your local `/software/quanser-fixed2021` (sub)directory and open the `setup_lab_heli_3d.m` file. 

8. Double check that in this `setup_lab_heli_3d.m` on line 27 you have correctly set `WITH_ADS = 'NO';` The possibility to set the option to `YES` is only included for possible extension of the whole laboratory model with some extra hardware (active disturbance system, ADS). Since we do not have it, you can also freely ignore the files with the ADS label in their names (such as the files  `setup_ads_configuration.m` and `q_heli3d_w_ads.mdl`).

9. Run the file. It will essentially build a mathematical model (by calling some other function from the directory) and design an LQR controller with an integral part. 

10. Once you are done, open the `q_heli3d.mdl` Simulink model that contains the blocks for interfacing the real experimental model with the just designed controller (note that the similarly named file `s_heli3d.mdl` is just a simulation version of the model).

11. Note that if you want to record the measured data for later analysis (which you will certainly do), the routes that you are familiar with from purely simulation projects may not work here. The reason is that while using the Quanser QUARC system, you are running the Simulink model in [External Mode](https://www.mathworks.com/help/sldrt/ug/simulink-external-mode.html), which prevents from using some Simulink functionality. In particular, if you [use Simulink scope block(s) for logging the data](https://www.mathworks.com/help/sldrt/ug/set-scope-parameters-for-logging-to-file.html), which is a common route, increasing the parameter called `Limit data points to last` will have no impact on how long the sequence of measurements will be stored in Matlab workspace (but do not forget to tick off the `Log data workspace` option and insert the variable name). If you ignore this issue, you may easily leave the lab assured that you stored a minute or two of experimental data only to discover at home that only the last few seconds have been saved. In order to prevent this, the `Duration` parameter needs to be changed somewhere else. Namely, go to manu `Code`, submenu `External Mode Control Panel`. Click on `Signal & Triggering` and change the `Duration` parameter there. If you need more on this, have a look at [the secion on Data Collection in QUARC manual](https://docs.quanser.com/quarc/documentation/quarc_data_collection.html) but if you are happy with logging to the variables in Matlab workspace through the Simulink scope blocks, you should be fine now. 

12. In the Hardware tab in Simulink, click the green triangle accompanied by "Monitor & Tune" text. This may take a few seconds and then the experiments starts. 

13. In case the experiment fails compiling and running, check that the Quanser block in the diagram is correctly configured to the Q8 USB board. Also, the target should be set (Ctrl+E) to `quanser_win64.tlc`. 

14. After the experiment starts running, the green triangle should turn into an icon (black square) for stopping the experiment. But if this does not happen (for whatever reason), the fallback solution for stopping the experiment is the "Stop all" option offered through the small Quanser icon at the bottom right panel in Windows.

15. Now you are ready to replace the internals of the controller with your own stuff. Good luck.

16. Honestly, the mathematical model created upon calling the `setup_heli_3d_configuration.m` file deservers some polishing too. Maybe a significant one... After all, it is just a linear model. If you want to read about one particular modeling endeavour which resulted in a nonlinear model, have a look at the paper Brentari, Mirko, Paolo Bosetti, Isabelle Queinnec, a Luca Zaccarian. „Benchmark model of Quanser’s 3 DOF Helicopter". LAAS CNRS, February 2018. https://hal.laas.fr/hal-01711135. 


## Some comments on interpreting the measurements

### Incremental encoders

Note that the angular sensors used in this setup are incremental encoders. This means that based solely on the measurements, the controller has no way to tell the initial angle. This simply means that whatever commanded (reference) value will be always regarded with respect to the initial angle. While this does not constitute a problem for the travel angle and the elevation problem, it may be a challenge for the pitch angle because the experiment can equally well start with the helicopter's nose up or down. 

### Incorrectly stated positive direction of travel angle in the documentation

Note that the documentation states the positive direction of the travel angle inconsistently with the measurements. The documentation says that the travel angle `λ` increases for counterclockwise motion, but this is not only in disagreement with the measurements but also does not comply with the conventions in aerospace. The travel angle in this experiment corresponds with the yaw angle `ψ` as used in the aerospace (the angle around the vertical body axis), for which [the common conventions is that it is positive when turning to the right](https://en.wikipedia.org/wiki/Flight_dynamics_(fixed-wing_aircraft)), which corresponds to the clockwise travel in the experiment. Until resolved, we keep it as an [issue](https://gitlab.fel.cvut.cz/aa4cc/edu-ctrl-lab-models/quanser-3dof-helicopter/-/issues/3).    

![Roll, pitch, yaw](figures/400px-Yaw_Axis_Corrected.png)

Source: [https://commons.wikimedia.org/wiki/File:Yaw_Axis_Corrected.svg](https://commons.wikimedia.org/wiki/File:Yaw_Axis_Corrected.svg)

### Names and labels for the other two angles

While the other two angles appear to be defined consistently with the measurements, it may still be worth emphasizing that the pitch angle labelled `ρ` here is actually much more commonly labelled `θ` in aerospace texts. The angle increases when the aircraft's nose is going up.

Similarly, the angle called tilt here and labelled `ε` is actually equivalent to the more conventional roll angle `φ` from aerospace texts. The angle increases when the aircraft's (possibly just imagined) right wing is going down.

While the experiments can be viewed and operated independently of notational conventions in aerospace, it may be convenient to stick to them.
