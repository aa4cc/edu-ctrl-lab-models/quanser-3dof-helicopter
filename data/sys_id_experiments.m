%% Sys ID using PRBS inputs – design of the input signals and preprocessing of the measurements

Ts = 0.002;         % Sampling period [s]
N = 2^16-1;         % Number of samples (should be perhaps modified so that full PRBS sequence is obtained for B != 1)
Tf = Ts*N           % Final time

%% Preparation of the input (voltage) signal
% Two major parameters for the PRBS signal:
Range = [-3.0,3.0];
Band = [0,1/4];

u0 = 5;     % Offset voltage (in experiments we manually iterated from 4 to 8 V)
uprbs = idinput(N,'prbs',Band,Range) + u0;
uprbs = repmat(uprbs,1,2);  % Two motors
t = linspace(0,Tf,N);
uprbs = timeseries(uprbs,t,'Name','PRBS voltage');  % timeseries object is needed for feeding the signal into Simulink simulation.

%% Loading and plotting the stored experimental data
% The code below is only useful once the experiments have been conducted and the responses saved.

load elevation_PRBS_response_U4_B4_1

%elevation_data = elevation_data(500:end,:);
plot(elevation_data(:,1),elevation_data(:,2));
y = elevation_data(:,2);
u = voltage_data(:,2);

%voltage_data = voltage_data(500:end,1:2);
subplot(2,1,1)
plot(elevation_data(:,1),elevation_data(:,2));
ylabel('Elevation [deg]')
subplot(2,1,2)
plot(voltage_data(:,1),voltage_data(:,2));
ylabel('Voltage [V]')

%% Creating IDDATA object for automatic system identification procedures

elevation_response_data = iddata(voltage_data(:,2),elevation_data(:,2),Ts,...
    'ExperimentName','Response of elevation to voltage inputs',...
    'InputUnit','V','InputName','Voltage',...
    'OutputUnit','deg','OutputName','Elevation');

%%
% Upon launching the <matlab:doc('systemIdentification')
% systemIdentification> GUI, the IDDATA object consisting of both input and
% output signal can be imported and processed.